package com.kshrd.avengerbio;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class HeroActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mIvIronman;
    private ImageView mIvCap;

    public final static int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero);

        mIvIronman = findViewById(R.id.hero_iv_ironman);
        mIvCap = findViewById(R.id.hero_iv_cap);

        mIvIronman.setOnClickListener(this);
        mIvCap.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.hero_iv_ironman:
                // go to second activity:
                Intent ironManIntent = new Intent(HeroActivity.this, BioActivity.class);
                ironManIntent.putExtra("name", "Iron Man");
                ironManIntent.putExtra("detail", "Iron Man is a fictional superhero appearing in American comic books published by Marvel Comics. The character was co-created by writer and editor Stan Lee, developed by scripter Larry Lieber, and designed by artists Don Heck and Jack Kirby.");
                startActivityForResult(ironManIntent, REQUEST_CODE);
                break;
            case R.id.hero_iv_cap:
                // go to second activity:
                Intent capIntent = new Intent(HeroActivity.this, BioActivity.class);
                capIntent.putExtra("name", "Captain America");
                capIntent.putExtra("detail", "Steve Rogers (Chris Evans) wants to do his part and join America.");
                startActivityForResult(capIntent, REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, data.getStringExtra("message"), Toast.LENGTH_LONG).show();
            }
        }
    }
}
