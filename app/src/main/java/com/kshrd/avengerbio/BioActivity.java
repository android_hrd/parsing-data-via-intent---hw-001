package com.kshrd.avengerbio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class BioActivity extends AppCompatActivity {

    private ImageView mIvHero;
    private TextView mTvName;
    private TextView mTvDetail;
    private EditText mEtMessage;
    private Button mBtSendBack;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio);

        mIvHero = findViewById(R.id.bio_iv_image);
        mTvName = findViewById(R.id.bio_tv_name);
        mTvDetail = findViewById(R.id.bio_tv_detail);
        mEtMessage = findViewById(R.id.bio_et_message);
        mBtSendBack = findViewById(R.id.bio_bt_send_back);

        extras = getIntent().getExtras();

        if (extras != null) {
            String name = extras.getString("name");
            String detail = extras.getString("detail");

            setUp(name, detail);
        }

        mBtSendBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = getIntent();
                backIntent.putExtra("message", mEtMessage.getText().toString());
                setResult(RESULT_OK, backIntent);
                finish();
            }
        });

    }

    private void setUp(String name, String detail) {
        if (name.equals("Iron Man")) {
            // we show iron man staff
            mIvHero.setImageDrawable(getResources().getDrawable(R.drawable.ironman));
        } else if (name.equals("Captain America")) {
            // we show cap staff
            mIvHero.setImageDrawable(getResources().getDrawable(R.drawable.capitan_america));
        }
        mTvName.setText(name);
        mTvDetail.setText(detail);
    }

}
